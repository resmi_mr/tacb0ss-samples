package com.nu.art.software.TacB0sS.samples.genericPreferences;


import android.app.Application;


/**
 * The main advantage is the encapsulation of all the storage logic, with type safe actions, while exposing a very
 * convenient API!
 *
 * @author TacB0sS
 */
public class AppSpecificPreferenceStorage
		extends PreferencesStorage {

	private final PreferenceKey<String> StringTypeKey = new PreferenceKey<String>("Private - This Key Saves String");

	private final PreferenceKey<Integer> PublicIntegerValue = new PreferenceKey<Integer>("Public - This Key Saves an int");

	private final PreferenceKey<Long> PublicLongValue = new PreferenceKey<Long>("Public - This Key Saves a long");

	private static AppSpecificPreferenceStorage instance;

	public static AppSpecificPreferenceStorage createSingleton(Application application) {
		if (instance != null)
			return instance;
		return instance = new AppSpecificPreferenceStorage(application);
	}

	public static AppSpecificPreferenceStorage getInstance() {
		if (instance == null)
			throw new IllegalStateException("MUST call createSingleton(...) from your Application onCreate(...) method!!");
		return instance;
	}

	private AppSpecificPreferenceStorage(Application application) {
		super(application, AppPreferences.Private);
	}

	public final void storePrivateString(String value) {
		putValue(StringTypeKey, value);
	}

	public final String getPrivateString() {
		return getValue(StringTypeKey, //
				"If I would ever need a default value"//
						+ " it would be here with the rest"//
						+ " of the default values I would ever use in the entire app!!!");
	}

	public final void storePublicInt(int value) {
		putValue(PublicIntegerValue, value);
	}

	public final int getPublicInt() {
		return getValue(PublicIntegerValue, -111);
	}

	/**
	 * The beauty of this is in the pre-compile type bug resolving... for example, try to replace <b>PublicLongValue</b>
	 * with <b>PublicIntegerValue</b>, or the other way around...
	 */
	public final void storePublicLong(long value) {
		putValue(PublicLongValue, value);
	}

	public final long getPublicLong() {
		return getValue(PublicLongValue, (long) -111);
	}

}
