package com.nu.art.software.TacB0sS.samples.genericPreferences;


import android.app.Application;
import android.content.SharedPreferences;

import com.nu.art.software.TacB0sS.samples.genericPreferences.PreferencesStorage.SharedPreferencesDetails;


/**
 * These represent the {@link SharedPreferences} details for the {@link PreferencesStorage} to use!
 *
 * @author TacB0sS
 */
public enum AppPreferences implements SharedPreferencesDetails {
	Private("Default", Application.MODE_PRIVATE),
	Default1("ForEveryoneElse", Application.MODE_WORLD_READABLE);

	private String preferencesName;

	private int mode;

	private AppPreferences(String name, int mode) {
		this.preferencesName = name;
		this.mode = mode;
	}

	@Override
	public String getPreferencesName() {
		return preferencesName;
	}

	@Override
	public int getMode() {
		return mode;
	}
}
