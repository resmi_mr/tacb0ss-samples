package com.nu.art.software.TacB0sS.samples;


import android.app.Application;
import android.util.Log;

import com.nu.art.software.TacB0sS.samples.genericPreferences.AppSpecificPreferenceStorage;


public class ExampleApplication
		extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		AppSpecificPreferenceStorage storage = AppSpecificPreferenceStorage.createSingleton(this);
		storage.storePrivateString("My little secret string...");
		storage.storePublicLong(453453);
		String privateString = storage.getPrivateString();
		long publicLong = storage.getPublicLong();
		Log.i("GenericPreferences", "String ... " + privateString);
		Log.i("GenericPreferences", "Public long ... " + publicLong);
	}
}
