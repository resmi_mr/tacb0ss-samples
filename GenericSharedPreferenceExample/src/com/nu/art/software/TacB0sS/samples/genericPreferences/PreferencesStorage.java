package com.nu.art.software.TacB0sS.samples.genericPreferences;


import java.util.HashMap;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


/**
 * I'm not going to document this follow the example.
 *
 * @author TacB0sS
 */
public abstract class PreferencesStorage {

	protected static final class PreferenceKey<Type> {

		private final String key;

		private final SharedPreferencesDetails type;

		public PreferenceKey(String key) {
			this(key, null);
		}

		public PreferenceKey(String key, SharedPreferencesDetails type) {
			this.key = key;
			this.type = type;
		}
	}

	public interface SharedPreferencesDetails {

		public String getPreferencesName();

		public int getMode();
	}

	private HashMap<SharedPreferencesDetails, SharedPreferences> preferencesMap = new HashMap<SharedPreferencesDetails, SharedPreferences>();

	private final Application application;

	private final SharedPreferencesDetails defaultPreferences;

	protected PreferencesStorage(Application application, SharedPreferencesDetails defaultPreferences) {
		this.application = application;
		this.defaultPreferences = defaultPreferences;
	}

	// @SuppressWarnings("unchecked")
	public final <Type> void putValue(PreferenceKey<Type> type, Type value) {
		Editor editor = getPreferences(type.type).edit();
		if (value instanceof Boolean)
			editor.putBoolean(type.key, (Boolean) value);
		else if (value instanceof String)
			editor.putString(type.key, (String) value);
		else if (value instanceof Float)
			editor.putFloat(type.key, (Float) value);
		else if (value instanceof Integer)
			editor.putInt(type.key, (Integer) value);
		else if (value instanceof Long)
			editor.putLong(type.key, (Long) value);
		// else if (value instanceof Set)
		// editor.putStringSet(type.key, (Set<String>) value);
		else
			throw new RuntimeException("Bad implementation... can only handle: Boolean, String, Float, Integer, Long, Set<String>!");
		editor.commit();
	}

	@SuppressWarnings("unchecked")
	public final <Type> Type getValue(PreferenceKey<Type> type, Type defaultValue) {
		SharedPreferences preferences = getPreferences(type.type);
		if (defaultValue instanceof Boolean)
			return (Type) (Boolean) preferences.getBoolean(type.key, (Boolean) defaultValue);

		if (defaultValue instanceof String || defaultValue == null)
			return (Type) preferences.getString(type.key, (String) defaultValue);

		if (defaultValue instanceof Float)
			return (Type) (Float) preferences.getFloat(type.key, (Float) defaultValue);

		if (defaultValue instanceof Integer)
			return (Type) (Integer) preferences.getInt(type.key, (Integer) defaultValue);

		if (defaultValue instanceof Long)
			return (Type) (Long) preferences.getLong(type.key, (Long) defaultValue);

		// if (defaultValue instanceof Set)
		// return (Type) preferences.getStringSet(type.key, (Set<String>) defaultValue);
		throw new RuntimeException("Bad implementation... can only handle: Boolean, String, Float, Integer, Long, Set<String>!");
	}

	private SharedPreferences getPreferences(SharedPreferencesDetails type) {
		if (type == null)
			type = defaultPreferences;

		SharedPreferences sharedPreferences = preferencesMap.get(type);
		if (sharedPreferences == null) {
			sharedPreferences = application.getSharedPreferences(type.getPreferencesName(), type.getMode());
			preferencesMap.put(type, sharedPreferences);
		}
		return sharedPreferences;
	}

	public void clearCache() {
		for (SharedPreferencesDetails key : preferencesMap.keySet()) {
			Editor edit = getPreferences(key).edit();
			edit.clear();
			edit.commit();
		}
	}
}
