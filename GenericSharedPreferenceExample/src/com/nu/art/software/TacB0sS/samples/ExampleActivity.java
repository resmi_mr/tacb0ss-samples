package com.nu.art.software.TacB0sS.samples;


import android.app.Activity;
import android.os.Bundle;

import com.nu.art.software.TacB0sS.samples.genericPreferences.R;


public class ExampleActivity
		extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.empty_activity);
	}
}
