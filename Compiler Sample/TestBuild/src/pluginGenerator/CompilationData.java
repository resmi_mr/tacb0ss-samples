package pluginGenerator;


import java.io.File;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;


public class CompilationData {

	private Writer writer;

	private String outputDirectory;

	private Vector<File> sourceFolders = new Vector<File>();

	private Vector<File> classPath = new Vector<File>();

	// private Vector<File> classes = new Vector<File>();
	//
	// public final void addClass(String _class) {
	// addClass(new File(_class));
	// }
	//
	// private void addClass(File _classFile) {
	// classes.add(_classFile);
	// }

	public final void addSource(String pathToSource) {
		addSourceDirectory(new File(pathToSource));
	}

	public final void addSourceDirectory(File source) {
		if (!source.isDirectory())
			throw new IllegalArgumentException("Source Directory MUST be a directory");
		sourceFolders.add(source);
	}

	public final void addToClassPath(String pathToFile) {
		addToClassPath(new File(pathToFile));
	}

	public final void addToClassPath(File classpathEntry) {
		sourceFolders.add(classpathEntry);
	}

	public final Writer getWriter() {
		return writer;
	}

	public final void setWriter(Writer writer) {
		this.writer = writer;
	}

	public final String getOutputDirectory() {
		return outputDirectory;
	}

	public final void setOutputDirectory(String outputDirectory) {
		this.outputDirectory = outputDirectory;
	}

	public File[] getClassPath() {
		return classPath.toArray(new File[classPath.size()]);
	}

	public File[] getSourcesFolders() {
		return sourceFolders.toArray(new File[sourceFolders.size()]);
	}

	public List<File> getSourceFiles() {
		ArrayList<File> files = new ArrayList<File>();
		for (int i = 0; i < sourceFolders.size(); i++) {
			getSourceFiles(files, sourceFolders.get(i));
		}
		return files;
	}

	private void getSourceFiles(ArrayList<File> files, File sourceFolder) {
		File[] listFiles = sourceFolder.listFiles();
		for (int i = 0; i < listFiles.length; i++) {
			if (listFiles[i].isDirectory()) {
				getSourceFiles(files, listFiles[i]);
				continue;
			}
			files.add(listFiles[i]);
		}
	}

}
