package pluginGenerator;


import javax.tools.DiagnosticCollector;
import javax.tools.JavaFileObject;


public final class CompilationException
		extends Exception {

	private static final long serialVersionUID = 6072693401647281420L;

	private final DiagnosticCollector<JavaFileObject> diagnosticCollector;

	public CompilationException(String message, DiagnosticCollector<JavaFileObject> diagnostics) {
		super(message);
		this.diagnosticCollector = diagnostics;
	}

	public DiagnosticCollector<JavaFileObject> getDiagnosticCollector() {
		return diagnosticCollector;
	}
}
