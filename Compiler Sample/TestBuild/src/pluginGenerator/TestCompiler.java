package pluginGenerator;


import java.util.List;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaFileObject;


public class TestCompiler {

	private static final String PathToRepository = "d:/Repositories/TacB0sS-Samples/Compiler Sample";

	public static void main(String[] args) {
		System.setProperty("java.home", "c:\\Program Files\\Java\\jdk1.6.0_45\\");
		CompilationData data = new CompilationData();
		data.setOutputDirectory("C:/output/");
		data.addSource(PathToRepository + "/Project A/src");
		data.addSource(PathToRepository + "/Project B/src");
		data.addSource(PathToRepository + "/Project C/src");
		Compiler compiler = new Compiler();
		try {
			compiler.compile(data);
		} catch (CompilationException e) {
			DiagnosticCollector<JavaFileObject> diagnosticCollector = e.getDiagnosticCollector();
			List<Diagnostic<? extends JavaFileObject>> diagnostics = diagnosticCollector.getDiagnostics();
			for (Diagnostic<? extends JavaFileObject> diagnostic : diagnostics) {
				System.out.println(diagnostic.getMessage(null));
			}
			e.printStackTrace();
		}
	}
}
