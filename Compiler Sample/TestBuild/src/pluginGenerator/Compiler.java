package pluginGenerator;


import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;


public final class Compiler {

	private final JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();

	private Locale locale;

	private Charset charSet;

	private StandardJavaFileManager fileManager;

	public Compiler() {}

	public final boolean compile(CompilationData data)
			throws CompilationException {
		DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<JavaFileObject>();
		fileManager = compiler.getStandardFileManager(diagnostics, locale, charSet);
		List<File> sourceFiles = data.getSourceFiles();
		Iterable<? extends JavaFileObject> compilationUnits = fileManager.getJavaFileObjectsFromFiles(sourceFiles);
		System.out.println("Compiling (" + sourceFiles.size() + ") Files... may take a while");

		long started = System.currentTimeMillis();

		compile(data, compilationUnits);

		System.out
				.println("Finished Compiling Files... process took: " + (System.currentTimeMillis() - started) + "ms");
		return true;

	}

	private void compile(CompilationData data, Iterable<? extends JavaFileObject> compilationUnits)
			throws CompilationException {
		ArrayList<String> options = new ArrayList<String>();
		options.add("-d");
		options.add(data.getOutputDirectory());
		options.add("-classpath");

		String classPath = "";
		for (File classpathEntry : data.getClassPath())
			classPath += ";" + classpathEntry.getAbsolutePath();

		options.add(classPath);
		DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<JavaFileObject>();
		if (!compiler.getTask(data.getWriter(), fileManager, diagnostics, options, null, compilationUnits).call())
			throw new CompilationException("Compilation Error", diagnostics);
	}

	public Locale getLocal() {
		return locale;
	}

	public void setLocal(Locale local) {
		this.locale = local;
	}

	public Charset getCharSet() {
		return charSet;
	}

	public void setCharSet(Charset charSet) {
		this.charSet = charSet;
	}

}
