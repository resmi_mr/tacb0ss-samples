package com.nu.art.software.TacB0sS.keystoreCertificationHashing;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.util.Hashtable;
import java.util.Set;

import javax.xml.bind.DatatypeConverter;


/*
 * BROUGHT TO YOU BY TacB0sS - Nu-Art Software
 *
 * Takes a keystore certificate and generate the hash of it, specifically I've made this for Facebook application registration.
 */
public class CertificationHashing {

	public static final String[] ExpectedParams = new String[]{"Path to KeyStore file", "KeyStore Password",
			"Certificate Alias #1", "Certificate Alias #2", "...", "Certificate Alias #N"};

	// "c:/Users/Adam/.android/debug.keystore" "android" "androiddebugkey"
	public static final String[] DefaultParams = new String[]{"c:/Users/Adam/.android/debug.keystore", "android",
			"androiddebugkey"};

	public static void main(String[] args)
			throws Exception {
		// args = DefaultParams;
		if (args.length == 0) {
			String string = "How to execute:\n Keystore-Certificate-Hashing.jar";
			for (int i = 0; i < ExpectedParams.length; i++) {
				string += " \"" + ExpectedParams[i] + "\"";
			}
			System.out.println(string);
			return;
		}

		System.out.println();
		System.out.println();
		System.out.println("******************************************");
		System.out.println("*  KeyStore certificate Hashing utility  *");
		System.out.println("*                                        *");
		System.out.println("*  Brought to you by: TacB0sS            *");
		System.out.println("*                                        *");
		System.out.println("*  Nu-Art Software                       *");
		System.out.println("******************************************");
		System.out.println();

		String pathToFile = args[0];
		File file = new File(pathToFile);
		if (!file.exists()) {
			System.err.println("KeyStore file does not exists: " + file.getAbsolutePath());
			return;
		}

		System.out.println("Loading KeyStore: " + file.getAbsolutePath());
		System.out.println();
		System.out.println();
		InputStream in;
		try {
			in = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			System.err.println("Error accessing file: " + file.getAbsolutePath());
			throw e;
		}
		KeyStore ks;
		try {
			ks = KeyStore.getInstance(KeyStore.getDefaultType());
		} catch (KeyStoreException e) {
			System.err.println("Error creating default KeyStore instance.");
			try {
				in.close();
			} catch (IOException e1) {
				System.err.println("Error closing file input stream.");
			}
			throw e;
		}

		char[] passphrase = args[1].toCharArray();
		try {
			ks.load(in, passphrase);
		} catch (NoSuchAlgorithmException e) {
			System.err.println("Error loading KeyStore: NoSuchAlgorithmException");
			throw e;
		} catch (CertificateException e) {
			System.err.println("Error loading KeyStore: CertificateException (Wrong Password?)");
			throw e;
		} catch (IOException e) {
			System.err.println("Error loading KeyStore: IOException");
			throw e;
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				System.err.println("Error closing file input stream.");
			}
		}

		if (args.length == 2 || args[2].toLowerCase().equals("all")) {
			String[] aliases = showAllAliases(ks);
			if (args.length == 2) {
				for (int i = 0; i < aliases.length; i++) {
					System.out.println("Alias #" + i + ": " + aliases[i]);
				}
				return;
			}
			String[] newArgs = new String[args.length + aliases.length - 1];
			System.arraycopy(args, 0, newArgs, 0, args.length);
			System.arraycopy(aliases, 0, newArgs, args.length - 1, aliases.length);
			args = newArgs;
		}

		printAliasesDetails(args, ks);
	}

	protected static void printAliasesDetails(String[] args, KeyStore ks)
			throws KeyStoreException, CertificateEncodingException, NoSuchAlgorithmException, Exception {
		for (int i = 2; i < args.length; i++) {
			Certificate key;
			try {
				key = ks.getCertificate(args[i]);
			} catch (KeyStoreException e) {
				System.err.println("KeyStore has not been initialized.");
				throw e;
			}

			byte[] bytes;
			try {
				bytes = key.getEncoded();
			} catch (CertificateEncodingException e) {
				System.err.println("Error encoding certificate.");
				throw e;
			}

			String hash = getHash(bytes);
			String md5 = doFingerprint(bytes, "MD5");
			String sha1 = doFingerprint(bytes, "SHA");
			System.out.println("---- '" + args[i] + "' Certificate");
			System.out.println("      Hash: " + hash);
			System.out.println("      MD5:  " + md5.toUpperCase());
			System.out.println("      SHA1: " + sha1.toUpperCase());
			System.out.println();
			System.out.println();
		}
	}

	@SuppressWarnings("unchecked")
	private static String[] showAllAliases(KeyStore ks)
			throws Exception {
		try {
			Field field = ks.getClass().getDeclaredField("keyStoreSpi");
			field.setAccessible(true);
			Object map = field.get(ks);

			Field field2 = map.getClass().getSuperclass().getDeclaredField("entries");
			field2.setAccessible(true);

			Hashtable<String, ?> entries = (Hashtable<String, ?>) field2.get(map);
			Set<String> aliasesSet = entries.keySet();
			return aliasesSet.toArray(new String[aliasesSet.size()]);
		} catch (Exception e) {
			System.err.println("Error encoding certificate.");
			throw e;
		}
	}

	private static String getHash(byte[] bytes)
			throws NoSuchAlgorithmException {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA");
		} catch (NoSuchAlgorithmException e) {
			System.err.println("Could not create MessageDigest of type 'SHA'");
			try {
				md = MessageDigest.getInstance("MD5");
			} catch (NoSuchAlgorithmException e1) {
				System.err.println("Could not create MessageDigest of type 'MD5'");
				throw e;
			}
		}

		md.update(bytes);
		return DatatypeConverter.printBase64Binary(md.digest());
	}

	protected static String doFingerprint(byte[] certificateBytes, String algorithm)
			throws Exception {
		MessageDigest md = MessageDigest.getInstance(algorithm);
		md.update(certificateBytes);
		byte[] digest = md.digest();

		String toRet = "";
		for (int i = 0; i < digest.length; i++) {
			if (i != 0)
				toRet += ":";
			int b = digest[i] & 0xff;
			String hex = Integer.toHexString(b);
			if (hex.length() == 1)
				toRet += "0";
			toRet += hex;
		}
		return toRet;
	}
}
