package com.nu.art.software.TacB0sS.samples.dialogFragment;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


public class MyDialogFragment
		extends DialogFragment {

	protected final String TAG = getClass().getSimpleName();

	private int titleId;

	private int bodyId;

	private int buttonId;

	public MyDialogFragment(int titleId, int bodyId, int buttonId) {
		super();
		this.titleId = titleId;
		this.bodyId = bodyId;
		this.buttonId = buttonId;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.alert_dialog_body, container);
		TextView title = (TextView) view.findViewById(R.id.Title);
		TextView body = (TextView) view.findViewById(R.id.AlertBody);
		Button button = (Button) view.findViewById(R.id.DialogButton);

		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				getDialog().dismiss();
			}
		});

		button.setText(buttonId);
		title.setText(titleId);
		body.setText(bodyId);
		return view;
	}

	@Override
	public void onAttach(android.app.Activity activity) {
		super.onAttach(activity);
	}

	@Override
	public void onCreate(android.os.Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NO_FRAME, R.style.AlertDialogStyle);
	}

}
