package com.nu.art.software.TacB0sS.regexBuddyHistoryExtractor;


import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;


public class WindowsReqistry {

	private static final String Key_Key = "${KEY}";

	private static final String EntryType_Key = "${REGISTRY_TYPE}";

	private static final String RegistryNodeRegex = "(?:\\s|\t)*" + Key_Key + "(?:\\s|\t)*" + EntryType_Key
			+ "(?:\\s|\t)*(.*?)\n";

	public enum RegistryEntryType {
		REG_SZ,
		REG_MULTI_SZ,
		REG_EXPAND_SZ,
		REG_DWORD,
		REG_QWORD,
		REG_BINARY,
		REG_NONE
	}

	private static final String readRegistry(String location, RegistryEntryType entry, String key)
			throws InterruptedException, IOException {
		location.replace("/", "\\");
		if (location.startsWith("\\"))
			location = location.substring(1);
		String string = "reg query " + '"' + location + "\" /v " + key;
		Process process = Runtime.getRuntime().exec(string);

		ProcessOutputReader reader = new ProcessOutputReader(process.getInputStream());
		reader.start();
		process.waitFor();
		reader.join();

		String output = reader.getResult();
		output = output.replace("\r\n", "\n");

		RegexAnalyzer regexAnalyzer = new RegexAnalyzer(RegistryNodeRegex.replace(Key_Key, key).replace(EntryType_Key,
				entry.name()));
		return regexAnalyzer.findRegex(1, 1, output);

	}

	public final static int readDWord(String location, String key)
			throws IOException, InterruptedException {
		String output = readRegistry(location, RegistryEntryType.REG_DWORD, key);
		return Integer.parseInt(output.replace("0x", ""), 16);
	}

	public final static String readGZ(String location, String key)
			throws IOException, InterruptedException {
		return readRegistry(location, RegistryEntryType.REG_SZ, key);
	}

	public final static byte[] readBinaries(String location, String key)
			throws IOException, InterruptedException {
		String output = readRegistry(location, RegistryEntryType.REG_BINARY, key);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);

		for (int i = 0; i < output.length(); i += 2) {
			dos.write(Integer.parseInt(output.substring(i, i + 2), 16));
		}
		return baos.toByteArray();
	}

	static class ProcessOutputReader
			extends Thread {

		private InputStream is;

		private StringWriter sw = new StringWriter();

		public ProcessOutputReader(InputStream is) {
			this.is = is;
		}

		@Override
		public void run() {
			try {
				int c;
				while ((c = is.read()) != -1)
					sw.write(c);
			} catch (IOException e) {}
		}

		public String getResult() {
			return sw.toString();
		}
	}

}
