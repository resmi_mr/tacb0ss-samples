/*
 * Copyright 2008-2013, 'Nu-Art Software' AND/OR 'Adam Zehavi AKA TacB0sS'
 *
 * -------------------  !!! THIS FILE IS NOT FOR PUBLIC EYES !!!  -------------------
 *
 * If you have obtained a source file with this header without proper authorization,
 *
 * please take the time and report this to: <Support@Nu-Art-Software.com>
 * ----------------------------------------------------------------------------------
 *
 * IF THIS HEADER IS ATTACHED TO ANY SOURCE/RESOURCE FILE, YOU ARE HERE BY INFORMED
 * THAT THE CONTENT OF THIS FILE IS A PROPERTY OF 'Nu-Art Software' AND/OR 'Adam Zehavi AKA TacB0sS.
 * VIEWING THIS CONTENT IS A VIOLATION OF OUR INTELECTUAL PROPERTIES AND/OR PRIVACY
 * UNLESS GIVEN A SPECIFIC WRITTEN AUTHORIZATION, AND/OR YOU HAVE SIGNED
 * 'Nu-Art Software' CYBORG FRAMEWORK NDA.
 * ----------------------------------------------------------------------------------
 *
 * If the above was not clear enough...
 * Without proper authorization, you are not allowed to do anything with this code...
 *
 * Do NOT:
 * VIEW IT !!			AND/OR
 * COPY IT !!			AND/OR
 * USE IT !!			AND/OR
 * DISTRIBUTE IT !!		AND/OR
 * CHANGE IT !!			AND/OR
 * AND/OR DO ANY OTHER THING WITH IT!!!
 *
 * [I'M SURE YOU GET THE PICTURE]
 * ----------------------------------------------------------------------------------
 *
 * Last Builder: TacB0sS
 */
package com.nu.art.software.TacB0sS.regexBuddyHistoryExtractor;


import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Find a regex in a string.
 *
 * @author TacB0sS
 */
public class RegexAnalyzer {

	public static final class RegexValidators {

		public static final String EmailRegexValidation = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	}

	/**
	 * Check if the supplied text contains any of the supplied Regex strings.
	 *
	 * @param caseSensitive Is looking for a case sensitive regex or not.
	 * @param fullText The full text to search for the supplied Regex array in.
	 * @param regexToFind An array of Regex to find any of them in the full text supplied.
	 * @return Whether the supplied text contains any of the supplied Regex.
	 */
	public static final boolean containsRegex(String fullText, boolean caseSensitive, String... regexToFind) {
		return RegexAnalyzer.findRegex(fullText, caseSensitive, false, regexToFind).length > 0;
	}

	private static final String[] findRegex(String fullText, boolean caseSensitive, boolean stopOnFirst, String... regexToFind) {
		Vector<String> toRet = new Vector<String>();
		RegexAnalyzer fr = new RegexAnalyzer("", caseSensitive);
		for (String regex : regexToFind) {
			fr.setRegex(regex);
			String[] instances = fr.instances(fullText);
			for (String instance : instances)
				toRet.add(instance);
			if (stopOnFirst && toRet.size() > 0)
				break;
		}
		return toRet.toArray(new String[toRet.size()]);
	}

	/**
	 * By default searches for all the Regex Instance for the first Regex Group, which is the entire Regex string
	 * supplied.
	 *
	 * @param caseSensitive Is looking for a case sensitive regex or not.
	 * @param fullText The full text to search for the supplied Regex array in.
	 * @param regexToFind An array of Regex to find any of them in the full text supplied.
	 * @return An array of the found results.
	 */
	public static final String[] findRegex(String fullText, boolean caseSensitive, String... regexToFind) {
		return RegexAnalyzer.findRegex(fullText, caseSensitive, false, regexToFind);
	}

	protected boolean caseSensitive = true;

	protected String counter = null;

	private String toFind;

	/**
	 * @param toFind The string to find.
	 */
	public RegexAnalyzer(String toFind) {
		setRegex(toFind);
	}

	/**
	 * Find a text in the output string
	 *
	 * @param toFind The string to find.
	 * @param caseSensitive Is looking for a case sensitive regex or not.
	 */
	public RegexAnalyzer(String toFind, boolean caseSensitive) {
		this(toFind);
		this.caseSensitive = caseSensitive;
	}

	/**
	 * @param instance The index of the instance for which to get the regex group.
	 * @param groupIndex The group index to get from the regex instance.
	 * @param fullText The full text to search for the regex in.
	 * @return The string of the specified group, from the specified instance of the complete regex.
	 */
	public final String findRegex(int instance, int groupIndex, String fullText) {
		String[] regexs = findRegex(instance, fullText, new int[]{groupIndex});
		if (regexs.length == 0)
			return null;
		return regexs[0];
	}

	/**
	 * @param instance The index of the instance for which to get the regex group.
	 * @param fullText The full text to search for the regex in.
	 * @param groupIndices The group indices to get from the regex instance.
	 * @return The strings of the specified group, from the specified instance of the complete regex.
	 */
	public final String[] findRegex(int instance, String fullText, int... groupIndices) {
		int instanceIndex = 1;
		Pattern p = caseSensitive ? Pattern.compile(toFind) : Pattern.compile(toFind, Pattern.CASE_INSENSITIVE);
		while (instanceIndex <= instance) {
			Matcher m = p.matcher(fullText);
			if (!m.find())
				break;
			if (instanceIndex == instance) {
				String[] toRet = new String[groupIndices.length];
				for (int i = 0; i < groupIndices.length; i++)
					toRet[i] = m.group(groupIndices[i]);
				return toRet;
			}
			instanceIndex++;
			fullText = fullText.replaceFirst(toFind, "");
		}
		return new String[0];
	}

	public String getCounter() {
		return counter;
	}

	/**
	 * @param fullText The text to check for the regex.
	 * @return The count of the instances of the full regex.
	 */
	public final int instanceCount(String fullText) {
		return instances(fullText).length;
	}

	/**
	 * @param fullText The text to check for the regex.
	 * @return The instances of the full regex.
	 */
	public final String[] instances(String fullText) {
		// int instanceIndex = 0;
		Vector<String> instances = new Vector<String>();
		String tempStr = "";
		Pattern p = caseSensitive ? Pattern.compile(toFind) : Pattern.compile(toFind, Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(fullText);
		while (true) {
			if (!m.find())
				break;
			tempStr = m.group(0);
			if (tempStr != null && tempStr.length() > 0)
				instances.add(tempStr);
			// fullText = fullText.replaceFirst(toFind, "");
		}
		return instances.toArray(new String[instances.size()]);
	}

	private void setRegex(String regex) {
		toFind = regex;
	}

}
