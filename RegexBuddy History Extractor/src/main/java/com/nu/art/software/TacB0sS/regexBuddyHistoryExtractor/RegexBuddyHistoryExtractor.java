package com.nu.art.software.TacB0sS.regexBuddyHistoryExtractor;


import java.util.Vector;

import com.nu.art.software.json.annotations.JSON_Class;
import com.nu.art.software.json.annotations.JSON_Field;
import com.nu.art.software.json.core.JSONer;


public class RegexBuddyHistoryExtractor {

	private static final String RegexBuddy_RegistryHistoryLocation = "HKEY_CURRENT_USER\\Software\\JGsoft\\RegexBuddy3\\History";

	@JSON_Class
	public static final class RegexBuddyHistory {

		@JSON_Field(
				listNodeType = RegexBuddyHistoryEntry.class)
		private Vector<RegexBuddyHistoryEntry> entries = new Vector<RegexBuddyHistoryEntry>();

		public final void addEntry(RegexBuddyHistoryEntry entry) {
			entries.add(entry);
		}

		public final void removeEntry(RegexBuddyHistoryEntry entry) {
			entries.remove(entry);
		}
	}

	@JSON_Class
	public static class RegexBuddyHistoryEntry {

		@JSON_Field
		private String label;

		@JSON_Field
		private String regex;

		public RegexBuddyHistoryEntry(String label, String regex) {
			super();
			this.label = label;
			this.regex = regex;
		}

		public RegexBuddyHistoryEntry() {}

		@Override
		public String toString() {
			return label + ": " + regex;
		}

	}

	private static RegexAnalyzer regexExtractor = new RegexAnalyzer("><regex>(.*?)</regex>");

	public static void main(String[] args) {
		try {
			RegexBuddyHistory regexBuddyHistory = new RegexBuddyHistory();
			int count = WindowsReqistry.readDWord(RegexBuddy_RegistryHistoryLocation, "Count");
			System.out.println("Count: " + count);

			for (int i = 0; i < count; i++) {
				RegexBuddyHistoryEntry entry = new RegexBuddyHistoryEntry();

				String label = WindowsReqistry.readGZ(RegexBuddy_RegistryHistoryLocation, "Label" + i);

				byte[] bytes = WindowsReqistry.readBinaries(RegexBuddy_RegistryHistoryLocation, "Action" + i);
				String value = new String(bytes);
				String regex = regexExtractor.findRegex(1, 1, value);

				System.out.println((i < 100 ? "0" : "") + (i < 10 ? "0" : "") + i + "= \"" + label + "\": " + regex);

				entry.label = label;
				entry.regex = regex;

				regexBuddyHistory.addEntry(entry);
			}
			JSONer jsoner = new JSONer();
			String outputAsJson = jsoner.serialize(regexBuddyHistory);
			System.out.println(outputAsJson);
		} catch (Exception e) {
			System.err.println(e);
			e.printStackTrace();
		}

		// // Sample usage
		// String value = WindowsReqistry.readRegistry("HKEY_CURRENT_USER\\Software\\JGsoft\\RegexBuddy3\\History",
		// RegistryNodeRegex);

	}
}
